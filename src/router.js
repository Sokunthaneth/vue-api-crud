import UserPage from "./components/UserPage.vue";
import WorldAPage from "./components/WorldAPage.vue";
import WorldBPage from "./components/WorldBPage.vue";
import WorldCPage from "./components/WorldCPage.vue";

const routes = [
    { path: '/', component: UserPage },
    { path: '/world-a', component: WorldAPage },
    { path: '/world-b', component: WorldBPage },
    { path: '/world-c', component: WorldCPage },
]

export default routes