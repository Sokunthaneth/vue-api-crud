import axios from "axios";

export default axios.create({
    // FAKE SERVER
    baseURL: "http://localhost:3200",
    headers: {
        "Content-type": "application/json"
    }
});
